﻿Imports System.Net.Mail
Imports System.Web
Imports Outlook = Microsoft.Office.Interop.Outlook


Module Email
  Sub SendEmail(ByVal subject As String, ByVal body As String, _
                Optional ByVal toEmail As String = "", _
                Optional ByVal ccEmail As String = "", _
                Optional ByVal AttachmentFileName As String = "")

    'SendEmailExchange(subject, body, toEmail, ccEmail, AttachmentFileName)

    SendEmailSMTP(subject, body, toEmail, ccEmail, AttachmentFileName)
  End Sub

  Sub SendEmailExchange(ByVal subject As String, ByVal body As String, _
                ByVal toEmail As String, _
                Optional ByVal ccEmail As String = "", Optional ByVal AttachmentFileName As String = "")

    'Create the Outlook application.
    Dim oApp As Outlook.Application = New Outlook.Application()
    'Create a new mail item.
    Dim message As Outlook.MailItem = oApp.CreateItem(Outlook.OlItemType.olMailItem)

    message.BCC = "pcbdata@pcbank.ca"

    'Attach the file if there is one
    Dim attach As Attachment = Nothing
    For Each tmpAttachment As String In AttachmentFileName.Split(";")
      If tmpAttachment <> "" Then
        message.Attachments.Add(tmpAttachment)
      End If
    Next

    'Add all the emails in the setting file, splitting it to individual emails
    message.To = ""
    'If toEmail.Length = 0 Then message.To = "PCBProductionSupport@pcbank.ca;gicsupport@pcbank.ca;Jason.Ilagan@pcbank.ca"


    'If there are any cc emails add them
    If ccEmail <> "" Then message.CC = ccEmail

    body = Replace(body, vbCrLf, "<br />")
    body = Replace(body, "&gt;", ">")
    body = Replace(body, "&lt;", "<")

    message.Subject = subject
    message.HTMLBody = body

    message.Send()

    If Not attach Is Nothing Then attach.Dispose()

  End Sub

  Sub SendEmailSMTP(ByVal subject As String, ByVal body As String, _
                Optional ByVal toEmail As String = "", _
                Optional ByVal ccEmail As String = "", _
                Optional ByVal AttachmentFileName As String = "")

    If toEmail.Length = 0 Then
      toEmail = "gicsupport@pcbank.ca" 'PCBProductionSupport@pcbank.ca;
    End If

    Dim message As MailMessage = New MailMessage()
    Dim attach As Attachment = Nothing

    message.From = New MailAddress("pcbdata@pcbank.ca")
    message.Bcc.Add("pcbdata@pcbank.ca")

    'Attach the file if there is one
    For Each tmpAttachment As String In AttachmentFileName.Split(";")
      If tmpAttachment <> "" Then
        attach = New Attachment(tmpAttachment)
        message.Attachments.Add(attach)
      End If
    Next

    'Add all the emails in the setting file, splitting it to individual emails
    For Each tmptoEmail As String In toEmail.Split(";")
      If Trim(tmptoEmail) <> "" Then message.To.Add(New MailAddress(tmptoEmail))
    Next

    'If there are any cc emails add them
    If ccEmail <> "" Then
      For Each tmpCCEmail As String In ccEmail.Split(";")
        If Len(tmpCCEmail) > 0 Then message.CC.Add(New MailAddress(tmpCCEmail))
      Next
    End If

    body = Replace(body, vbCrLf, "<br />")
    body = Replace(body, "&gt;", ">")
    body = Replace(body, "&lt;", "<")

    message.Subject = subject
    message.Body = body
    message.IsBodyHtml = True

    Dim client As SmtpClient = New SmtpClient("LCLSMTP.ngco.com")
    client.UseDefaultCredentials = True
    client.Send(message)

    If Not attach Is Nothing Then attach.Dispose()

  End Sub

  Sub SendErrorEmail(ByVal subject As String, ByVal body As String)
    SendEmail("Error - " + subject, "Error: <br/>" + body, "")
  End Sub

End Module

