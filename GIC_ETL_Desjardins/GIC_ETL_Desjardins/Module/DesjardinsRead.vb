﻿Imports Syncfusion.XlsIO
Imports System.Text
Imports GIC_ETL_Desjardins.GICDeposits

Module DesjardinsRead
  Private ex As Exception
  Private exMessage As String
  Private exSource As String = "Import"
  Private _outputFile As String
  Private _outputPath As String = "\\ngco.com\dfsroot01\DFSPCB\DFSPCB05\Decision Support\Data Group\Data\GIC\Desjardins\ToCannex\"
  'Private _outputPath As String = "D:\Temp\"

  Public Function outputFile() As String
    Return _outputFile
  End Function

  Public Function outputPath() As String
    Return _outputPath
  End Function

  Public Sub readFromExcel(ByVal fileName As String, ByVal localPath As String)

    If Not RegularExpressions.Regex.IsMatch(fileName, "CPG_PCB_202[0-9]-[0-1][0-9]-[0-3][0-9].xls") _
       Or Not IsDate(fileName.Substring(8, 10)) Then
      exMessage = "Invalid File Name """ & fileName & """"
      ex = New Exception(exMessage)
      ex.Source = exSource
      Throw ex
    End If


    Dim fileDate As Date = Date.Parse(fileName.Substring(8, 10))

    'Step 1 : Instantiate the spreadsheet creation engine.
    Dim excelEngine As ExcelEngine = New ExcelEngine()

    'Step 2 : Instantiate the excel application object.
    Dim myApplication As IApplication = excelEngine.Excel

    Dim myWorkbookIn As IWorkbook = myApplication.Workbooks.OpenReadOnly(localPath & fileName)
    Dim myWorkSheetIn As IWorksheet = myWorkbookIn.Worksheets(0)

    Dim transDate As Date
    Try
      transDate = myWorkSheetIn.Range(6, 4).Value
    Catch e As Exception
      exMessage = "Invalid Transaction date."
      exMessage += ("<br/>" & e.Message)
      ex = New Exception(exMessage)
      ex.Source = exSource
      Throw ex
    End Try
    If transDate.DayOfWeek = DayOfWeek.Saturday _
       Or transDate.DayOfWeek = DayOfWeek.Sunday Then
      exMessage = String.Format("Transaction Date: {0} is {1}<br/>", transDate.ToString("d"), transDate.ToString("dddd"))
      ex = New Exception(exMessage)
      ex.Source = exSource
      Throw ex
    End If

    If DateDiff(DateInterval.Day, fileDate, transDate) <> 0 Then
      exMessage = "Invalid Transaction Date."
      ex = New Exception(exMessage)
      ex.Source = exSource
      Throw ex
    End If

    Dim issueDate As Date
    Try
      issueDate = myWorkSheetIn.Range(7, 4).Value
    Catch e As Exception
      exMessage = "Invalid Settlement date."
      exMessage += ("<br/>" & e.Message)
      ex = New Exception(exMessage)
      ex.Source = exSource
      Throw ex
    End Try
    If issueDate.DayOfWeek = DayOfWeek.Saturday _
       Or issueDate.DayOfWeek = DayOfWeek.Sunday Then
      exMessage = String.Format("Settlement Date: {0} is {1}<br/>", issueDate.ToString("d"), issueDate.ToString("dddd"))
      ex = New Exception(exMessage)
      ex.Source = exSource
      Throw ex
    End If

    Dim deposits As New Deposits
    For year As Integer = 1 To 5
      deposits.maturityDates(year) = myWorkSheetIn.Range(7 + year, 8).Value
    Next
    deposits.issueDate = issueDate

    Dim row As Integer = 21

    Do While row <= 1000 _
          Or myWorkSheetIn.Range(row, 1).Value.Length > 0 _
          Or myWorkSheetIn.Range(row, 3).Value.Length > 0
      If myWorkSheetIn.Range(row, 1).Value.Length > 0 _
          Or myWorkSheetIn.Range(row, 3).Value.Length > 0 Then
        If myWorkSheetIn.Range(row, 3).Value.Length > 0 Then
          Dim deposit As New Deposit
          deposit.registration = ""
          deposit.accountNumber = myWorkSheetIn.Range(row, 1).Value
          deposit.amount = myWorkSheetIn.Range(row, 3).Value
          deposit.termYears = myWorkSheetIn.Range(row, 5).Value

          Select Case myWorkSheetIn.Range(row, 7).Value.Trim
            Case "Annual - End of Term"
              deposit.interestCompounding = "A"
            Case Else
              deposit.interestCompounding = "N"
          End Select

          Select Case myWorkSheetIn.Range(row, 7).Value.Trim
            Case "Non Compound - Annual"
              deposit.interestPayment = "A"
            Case "Annual - End of Term"
              deposit.interestPayment = "E"
            Case "Non Compound - Semi Annual"
              deposit.interestPayment = "S"
            Case "Non Compound - Monthly"
              deposit.interestPayment = "M"
          End Select

          deposit.redeemability = "N"

          deposit.issueDate = issueDate
          deposit.maturityDate = myWorkSheetIn.Range(row, 8).Value
          deposit.agent = 2040
          deposit.iaCode = " " 'myWorkSheetIn.Range(row, 2).Value
          deposit.maturityInstructions = "24"
          deposit.maturityBankingInfo = "81598000 2002087"

          If myWorkSheetIn.Range(row, 10).Value = "" Or myWorkSheetIn.Range(row, 10).Value = "0" Then
            deposit.rate = myWorkSheetIn.Range(row, 6).Value
            deposit.commissionRate = 0.0025
          ElseIf IsNumeric(myWorkSheetIn.Range(row, 10).DisplayText) Then
            deposit.rate = myWorkSheetIn.Range(row, 6).Value
            deposit.commissionAccountTaken = myWorkSheetIn.Range(row, 10).DisplayText
            'deposit.rate = myWorkSheetIn.Range(row, 10).Value
            'deposit.commissionRate = 0.0025D - (Decimal.Parse(myWorkSheetIn.Range(row, 10).Value) - Decimal.Parse(myWorkSheetIn.Range(row, 6).Value))
          End If

          deposit.securityCode = myWorkSheetIn.Range(row, 11).Value

          deposits.add(deposit)
        End If

      End If
      row += 1
    Loop

    Dim total As Decimal
    Try
      total = myWorkSheetIn.Range(8, 4).Value
    Catch e As Exception
      exMessage = "Invalid Total Amount."
      exMessage += ("<br/>" & e.Message)
      ex = New Exception(exMessage)
      ex.Source = exSource
      Throw ex
    End Try

    If total <> deposits.total Then
      exMessage = "Total Amount (Summary) does not equal Sum of QTY."
      ex = New Exception(exMessage)
      ex.Source = exSource
      Throw ex
    End If

    deposits.outputCannexCSV(outputPath)
    _outputFile = deposits.outputFile

  End Sub

  Public Sub sftpToCannex(ByVal fileName As String)

    Dim sftp As Sftp = New Sftp()
    sftp.localPath = outputPath()
    'sftp.remotePath = "/Usr/DSFPCB/CFN/IN/"
    sftp.remotePath = "/IN/"
    sftp.username = "DSFPCB"
    sftp.password = "2LYJUGBV"
    sftp.address = "ftp.cannex.com"
    sftp.Upload(fileName)

  End Sub
End Module
