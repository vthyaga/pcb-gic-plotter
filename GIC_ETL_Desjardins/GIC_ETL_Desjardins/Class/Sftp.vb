﻿Imports SBSftpCommon
Imports SBSimpleSftp

Class Sftp

  Private Client As TElSimpleSFTPClient

  Private _remotePath As String
  'Private _remoteName As String
  Private _localPath As String
  'Private _localName As String
  Private _username As String
  Private _password As String
  Private _address As String
  Private _port As Integer = 22

  Private ex As Exception
  Private _exMessage As String = ""
  Private exSource As String = "SFtp"


  Public Function Download(ByVal remoteName As String) As String
    If Not Client.Active Then Open()

    Dim localNameFull As String = _localPath & remoteName
    Dim remoteNameFull As String = _remotePath & remoteName

    If IO.File.Exists(localNameFull) Then
      _exMessage = "File """ & remoteName & """ already exists in local drive "
      _exMessage += ("""" & _localPath & """")
      'ex = New Exception(exMessage)
      'ex.Source = exSource
      'Throw ex
    Else
      Client.DownloadFile(remoteNameFull, localNameFull, TSBSFTPFileTransferMode.ftmSkip)
      Client.RemoveFile(remoteNameFull)
    End If

    Return localNameFull
  End Function

  Public Sub Upload(ByVal localName As String)
    If Not Client.Active Then Open()

    Dim localNameFull As String = _localPath & localName
    Dim remoteNameFull As String = _remotePath & localName

    Client.UploadFile(localNameFull, remoteNameFull, TSBSFTPFileTransferMode.ftmOverwrite)

  End Sub

  Public Function List() As ArrayList
    Dim Handle As Byte() = Nothing
    Dim AListing As New ArrayList

    Try
      If Not Client.Active Then Open()
      Handle = Client.OpenDirectory(_remotePath)
      AListing = New ArrayList
      Client.ReadDirectory(Handle, AListing)
    Catch e As Exception
      If _exMessage.Length > 0 Then _exMessage += "<br/><br/>"
      _exMessage += e.Message
    Finally
      If Not Handle Is Nothing Then
        Client.CloseHandle(Handle)
      End If
    End Try

    For i As Integer = (AListing.Count - 1) To 0 Step -1
      Dim obj As Object = AListing.Item(i)
      Dim localName = _localPath & (CType(obj, TElSftpFileInfo).Name)

      If IO.File.Exists(localName) Then
        If _exMessage.Length > 0 Then _exMessage += "<br/><br/>"
        _exMessage += "File """ & (CType(obj, TElSftpFileInfo).Name) & """ already exists in local drive "
        _exMessage += ("""" & _localPath & """")

        AListing.RemoveAt(i)
      End If
    Next

    'If exMessage.Length > 0 Then
    'ex = New Exception(exMessage)
    'ex.Source = exSource
    'Throw ex
    'End If

    Return AListing

  End Function

  Private Sub m_Client_OnKeyValidate(ByVal Sender As Object, ByVal ServerKey As SBSSHKeyStorage.TElSSHKey, ByRef Validate As Boolean)
    System.Console.WriteLine("Server key received")
    Validate = True ' NEVER do this. You MUST check the key somehow
  End Sub

  Public Sub New()

    SBUtils.Unit.SetLicenseKey("81E859A1826B6AE23F30FE755D3962B56F6E8E3E2B573B0E268202DD6414677EB596345B536B19D2130D9FE3D8B957CEAC37DDF1651E5ED361F0E67367A11AE846C0D632E820558E454BD3161E85D35F2D1E42B4887BEC135188D6A85EEF44FA03E8F4290E9413EBECEE06B00E62DF76724DF271DBAB2D9F2DF4C593C577B29767AE5972208A1021EF58EB01F8A1CB45A947D5712CD5A15C1C72C3E09DC48F277B5F7AFC7E8AA60C6EB9B1C5FB0936F099A816CC2873083A5F82FD4B25B2FE3559E14EEE1FFA8150784F6CDCD01F37121E6E9CC3A99FA9FF52E31E4D3F16A38143DBD8019C1F0AB6B4F266B869E503BA684BB8E3C9BF28ED69C4F230D4B5A1FC")

    Client = New TElSimpleSFTPClient
    AddHandler Client.OnKeyValidate, AddressOf m_Client_OnKeyValidate

  End Sub

  Public Sub Open()
    Try
      Client.Open()
    Catch E As Exception
      '_exMessage = "SFtp Connection failed."
      '_exMessage += "<br/>" & E.Message
      ex = New Exception("SFtp Connection failed.")
      ex.Source = exSource
      Throw ex
      Try
        Client.Close(True)
      Catch
      End Try
      Return
    End Try
  End Sub

  Protected Overrides Sub Finalize()
    Try
      Client.Close(True)
    Catch
    End Try
  End Sub

  ReadOnly Property exMessage() As String
    Get
      Return _exMessage
    End Get

  End Property

  Property remotePath() As String
    Get
      Return _remotePath
    End Get
    Set(ByVal value As String)
      _remotePath = value
    End Set
  End Property

  Property localPath() As String
    Get
      Return _localPath
    End Get
    Set(ByVal value As String)
      _localPath = value
    End Set
  End Property

  Property username() As String
    Get
      Return _username
    End Get
    Set(ByVal value As String)
      _username = value
      Client.Username = _username
    End Set
  End Property

  Property password() As String
    Get
      Return _password
    End Get
    Set(ByVal value As String)
      _password = value
      Client.Password = _password
    End Set
  End Property

  Property address() As String
    Get
      Return _address
    End Get
    Set(ByVal value As String)
      _address = value
      Client.Address = _address
    End Set
  End Property

  Property port() As Integer
    Get
      Return _port
    End Get
    Set(ByVal value As Integer)
      _port = value
      Client.Port = _port
    End Set
  End Property

End Class
