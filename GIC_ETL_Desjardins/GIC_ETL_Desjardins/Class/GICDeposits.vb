﻿Imports System.IO
Imports System.Text

Namespace GICDeposits

  Public Class Deposits
    Private ex As Exception
    Private exMessage As String = ""
    Private exSource As String = "Deposit"

    Private _deposits As New Collection
    Private _item As Integer
    Private _issueDate As Date
    Private _outputFile As String

    Private _maturityDates(5) As Date

    Public Function add(ByVal deposit As Deposit) As Deposit
      If _deposits.Contains(deposit.orderNumber) Then
        Return Nothing
      Else
        _item += 1
        deposit.item = _item
        deposit._parent = Me
        deposits.Add(deposit)

        Return deposit
      End If
    End Function

    Public Sub outputCannexCSV(ByVal path As String)
      Call verify()

      _outputFile = "TERMO" & _issueDate.ToString("yyyyMMdd") & ".txt"
      Dim newFile As FileStream = File.Create(path & _outputFile)
      Dim objReader As StreamWriter = New StreamWriter(newFile)
      objReader.WriteLine("Order Number,Issuer Code,Issuer Name,Registration,Account Number,Security Code,Amount,Term Years,Term Days,Interest Compounding,Interest Payment,Redeemability,Cashable Term in Days,Rate,Issue Date,Maturity Date,Agent#,IA Code,Client Name 1,Client Name 2,Commission Amount Taken,Maturity Instructions,Maturity Banking Info,Client Name Type,Currency,Client Name 3,Name4,Ownership Type,Term Months")

      Dim line As StringBuilder
      For Each deposit As Deposit In _deposits
        line = New StringBuilder()
        line.Append(deposit.orderNumber & ",")
        line.Append(deposit.issuerCode & ",")
        line.Append(deposit.issuerName & ",")
        line.Append(deposit.registration & ",")
        line.Append(deposit.accountNumber & ",")
        line.Append(deposit.securityCode & ",")
        line.Append(deposit.amount & ",")
        line.Append(deposit.termYears & ",")
        line.Append(deposit.termDays.ToString("#") & ",")
        line.Append(deposit.interestCompounding & ",")
        line.Append(deposit.interestPayment & ",")
        line.Append(deposit.redeemability & ",")
        line.Append(deposit.cashableTermInDays.ToString("#") & ",")
        line.Append((deposit.rate * 100).ToString("0.0000") & ",")
        line.Append(deposit.issueDate & ",")
        line.Append(deposit.maturityDate & ",")
        line.Append(deposit.agent & ",")
        line.Append(deposit.iaCode & ",")
        line.Append(deposit.clientName1 & ",")
        line.Append(deposit.clientName2 & ",")
        line.Append(deposit.commissionAccountTaken.ToString("0.##") & ",")
        line.Append(deposit.maturityInstructions & ",")
        line.Append(deposit.maturityBankingInfo & ",")
        line.Append(deposit.clientNameType & ",")
        line.Append(deposit.currency & ",")
        line.Append(deposit.clientName3 & ",")
        line.Append(deposit.name4 & ",")
        line.Append(deposit.ownershipType & ",")
        line.Append(deposit.termMonths.ToString("#"))

        objReader.WriteLine(line.ToString())
      Next

      objReader.Close()

    End Sub

    Private Sub verify()

      For Each deposit As Deposit In _deposits
        Dim err As String = deposit.err
        If err.Length > 0 Then
          exMessage += String.Format("Item {0} / Account Number {1} Error:", deposit.item, deposit.accountNumber)
          exMessage += String.Format("<br/>{0}<br/>", deposit.err)
        End If
      Next

      If exMessage.Length > 0 Then
        ex = New Exception(exMessage)
        ex.Source = exSource
        Throw ex
      End If
    End Sub

    ReadOnly Property deposits() As Collection
      Get
        Return _deposits
      End Get
    End Property

    ReadOnly Property total() As Decimal
      Get
        Dim _total As Decimal = 0

        For Each dep As Deposit In _deposits
          _total += dep.amount
        Next

        Return _total
      End Get
    End Property

    Property maturityDates(ByVal year As Integer) As Date
      Get
        Return _maturityDates(year)
      End Get

      Set(ByVal value As Date)
        _maturityDates(year) = value
      End Set

    End Property

    Property issueDate() As Date
      Get
        Return _issueDate.ToString("yyyyMMdd")
      End Get

      Set(ByVal value As Date)
        _issueDate = value
      End Set

    End Property

    ReadOnly Property outputFile() As String
      Get
        Return _outputFile
      End Get
    End Property

  End Class

  Public Class Deposit

    'Private _orderNumber As String
    Private _issuerCode As String = "PCB"
    Private _issuerName As String = "President's Choice Bank"
    Private _registration As String
    Private _accountNumber As String
    Private _securityCode As String
    Private _amount As Decimal
    Private _termYears As Integer
    Private _termDays As Integer
    Private _interestCompounding As String = ""
    Private _interestPayment As String
    Private _redeemability As String
    Private _cashableTermInDays As Integer
    Private _rate As Decimal
    Private _issueDate As Date
    Private _maturityDate As Date
    Private _agent As Integer
    Private _iaCode As String
    Private _clientName1 As String
    Private _clientName2 As String
    Private _commissionAccountTaken As Decimal
    Private _maturityInstructions As String
    Private _maturityBankingInfo As String
    Private _clientNameType As String
    Private _currency As String = "CAD"
    Private _clientName3 As String
    Private _name4 As String
    Private _ownershipType As String
    Private _termMonths As Integer
    Private _err As String = ""
    Private _item As Integer
    Private _commissionRate As Decimal
    Friend _parent As Deposits

    Property item() As Integer
      Get
        Return _item
      End Get

      Friend Set(ByVal value As Integer)
        _item = value
      End Set
    End Property

    Property err() As String
      Get
        Dim newErr As String = _err
        'compulsory fields
        If _interestCompounding.Length = 0 Then
          newErr += "Interest Description is Invalid.<br/>"
        End If

        If _accountNumber.Length = 0 Then
          newErr += "Account Number missing.<br/>"
        End If

        If _iaCode.Length = 0 Then
          newErr += "IA Code missing.<br/>"
        End If

        If _securityCode.Length = 0 Then
          'newErr += "Security Code missing.<br/>"
        End If

        If _termYears = Nothing Or _termYears = 0 Then
          newErr += "Term (Years) missing.<br/>"
        End If

        If _rate = Nothing Or _rate = 0 Then
          newErr += "Rate missing.<br/>"
        End If

        'If _commissionRate = Nothing Then
        'newErr += "Commission Rate missing.<br/>"
        'End If

        'validate fields
        If _maturityDate <> Nothing And _termYears <> Nothing Then
          If _parent.maturityDates(_termYears) <> _maturityDate Then
            'newErr += "Maturity Date does not match the date in the summary section.<br/>"
          End If
        End If

        If _issueDate <> Nothing And _maturityDate <> Nothing And _termYears <> Nothing Then
          If (DateDiff(DateInterval.DayOfYear, _issueDate, _maturityDate) / 365) < _termYears Then
            newErr += "Maturity Date is earlier than the term end.<br/>"
          End If
        End If

        If _issueDate <> Nothing And _maturityDate <> Nothing And _termYears <> Nothing Then
          If (DateDiff(DateInterval.DayOfYear, _issueDate, _maturityDate) / 365) > (_termYears + 1) Then
            newErr += "Maturity Date passes the term end.<br/>"
          End If
        End If

        If _commissionRate < 0 Then
          newErr += "Adjusted Interest Rate is too high (Commission given up is higher than 25 basis points).<br/>"
        End If

        If _commissionRate > 0.0025 Then
          newErr += "Adjusted Interest Rate is too low (commission rate > 0.0025).<br/>"
        End If

        Return newErr
      End Get

      Friend Set(ByVal value As String)
        _err = value
      End Set
    End Property

    ReadOnly Property orderNumber() As String
      Get
        Return String.Format("{0}{1}{2}{3}" _
                             , _issueDate.ToString("yy") _
                             , Chr(_issueDate.Month + Asc("A") - 1) _
                             , _issueDate.ToString("dd") _
                             , _item.ToString.PadLeft(5, "0"))
      End Get
    End Property

    ReadOnly Property issuerCode() As String
      Get
        Return _issuerCode
      End Get
    End Property

    ReadOnly Property issuerName() As String
      Get
        Return _issuerName
      End Get
    End Property

    Property registration() As String
      Get
        Return _registration
      End Get
      Set(ByVal value As String)
        _registration = value
      End Set
    End Property

    Property accountNumber() As String
      Get
        Return _accountNumber
      End Get
      Set(ByVal value As String)
        _accountNumber = value
      End Set
    End Property

    Property securityCode() As String
      Get
        Return _securityCode
      End Get
      Set(ByVal value As String)
        _securityCode = value
      End Set
    End Property

    Property amount() As String
      Get
        Return _amount.ToString("0.00")
      End Get

      Set(ByVal value As String)
        Try
          _amount = value
        Catch e As Exception
          _err += String.Format("Amount: {0}<br/>", e.Message)
        End Try
      End Set

    End Property

    Property termYears() As String
      Get
        Return _termYears.ToString("#")
      End Get
      Set(ByVal value As String)
        Try
          _termYears = value
        Catch e As Exception
          _err += String.Format("Term Years: {0}<br/>", e.Message)
        End Try
      End Set
    End Property

    Property termDays() As Integer
      Get
        Return _termDays
      End Get
      Set(ByVal value As Integer)
        _termDays = value
      End Set
    End Property

    Property interestCompounding() As String
      Get
        Return _interestCompounding
      End Get
      Set(ByVal value As String)
        _interestCompounding = value
      End Set
    End Property

    Property interestPayment() As String
      Get
        Return _interestPayment
      End Get
      Set(ByVal value As String)
        _interestPayment = value
      End Set
    End Property

    Property redeemability() As String
      Get
        Return _redeemability
      End Get
      Set(ByVal value As String)
        _redeemability = value
      End Set
    End Property

    Property cashableTermInDays() As Integer
      Get
        Return _cashableTermInDays
      End Get
      Set(ByVal value As Integer)
        _cashableTermInDays = value
      End Set
    End Property

    Property rate() As String
      Get
        Return _rate
      End Get
      Set(ByVal value As String)
        Try
          _rate = value
        Catch e As Exception
          _err += String.Format("Rate: {0}<br/>", e.Message)
        End Try
      End Set
    End Property

    Property commissionRate() As String
      Get
        Return _commissionRate
      End Get
      Set(ByVal value As String)
        Try
          _commissionRate = value
        Catch e As Exception
          _err += String.Format("Commission Rate: {0}<br/>", e.Message)
        End Try
      End Set
    End Property

    Property issueDate() As String
      Get
        Return _issueDate.ToString("yyyyMMdd")
      End Get
      Set(ByVal value As String)
        Try
          _issueDate = value
        Catch e As Exception
          _err += String.Format("Issue Date: {0}<br/>", e.Message)
        End Try

        If _issueDate.DayOfWeek = DayOfWeek.Saturday _
           Or _issueDate.DayOfWeek = DayOfWeek.Sunday Then
          _err += String.Format("Issue Date: {0} is {1}<br/>", _issueDate.ToString("d"), _issueDate.ToString("dddd"))
        End If
      End Set

    End Property

    Property maturityDate() As String
      Get
        Return _maturityDate.ToString("yyyyMMdd")
      End Get
      Set(ByVal value As String)
        Try
          _maturityDate = value
        Catch e As Exception
          _err += String.Format("Maturity Date: {0}<br/>", e.Message)
        End Try

        If _maturityDate.DayOfWeek = DayOfWeek.Saturday _
           Or _maturityDate.DayOfWeek = DayOfWeek.Sunday Then
          _err += String.Format("Maturity Date: {0} is {1}<br/>", _maturityDate.ToString("d"), _maturityDate.ToString("dddd"))
        End If
      End Set
    End Property

    Property agent() As Integer
      Get
        Return _agent
      End Get
      Set(ByVal value As Integer)
        _agent = value
      End Set
    End Property

    Property iaCode() As String
      Get
        Return _iaCode
      End Get
      Set(ByVal value As String)
        _iaCode = value
      End Set
    End Property

    Property clientName1() As String
      Get
        Return _clientName1
      End Get
      Set(ByVal value As String)
        _clientName1 = value
      End Set
    End Property

    Property clientName2() As String
      Get
        Return _clientName2
      End Get
      Set(ByVal value As String)
        _clientName2 = value
      End Set
    End Property

    Property commissionAccountTaken() As Decimal
      Get
        Return _commissionAccountTaken
      End Get
      Set(ByVal value As Decimal)
        Try
          _commissionAccountTaken = value
        Catch e As Exception
          _err += String.Format("commissionAccountTaken: {0}<br/>", e.Message)
        End Try
      End Set

      'Get
      ' Dim returnVal As Decimal

    'returnVal = Math.Round(_amount * _termYears * _commissionRate, 2, MidpointRounding.AwayFromZero)

    'Dim extraDays As Integer = DateDiff(DateInterval.Day, DateAdd(DateInterval.Year, _termYears, _issueDate), _maturityDate)

    'returnVal = Math.Round(_amount * (_termYears) * _commissionRate, 2, MidpointRounding.AwayFromZero) + _
    'Math.Round(_amount * (extraDays / 365) * _commissionRate, 2, MidpointRounding.AwayFromZero)

      'Return returnVal
      'End Get
    End Property

    Property maturityInstructions() As String
      Get
        Return _maturityInstructions
      End Get
      Set(ByVal value As String)
        _maturityInstructions = value
      End Set
    End Property

    Property maturityBankingInfo() As String
      Get
        Return _maturityBankingInfo
      End Get
      Set(ByVal value As String)
        _maturityBankingInfo = value
      End Set
    End Property

    Property clientNameType() As String
      Get
        Return _clientNameType
      End Get
      Set(ByVal value As String)
        _clientNameType = value
      End Set
    End Property

    ReadOnly Property currency() As String
      Get
        Return _currency
      End Get
    End Property

    Property clientName3() As String
      Get
        Return _clientName3
      End Get
      Set(ByVal value As String)
        _clientName3 = value
      End Set
    End Property

    Property name4() As String
      Get
        Return _name4
      End Get
      Set(ByVal value As String)
        _name4 = value
      End Set
    End Property

    Property ownershipType() As String
      Get
        Return _ownershipType
      End Get
      Set(ByVal value As String)
        _ownershipType = value
      End Set
    End Property

    Property termMonths() As Integer
      Get
        Return _termMonths
      End Get
      Set(ByVal value As Integer)
        _termMonths = value
      End Set
    End Property

  End Class
End Namespace