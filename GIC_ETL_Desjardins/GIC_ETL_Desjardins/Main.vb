﻿Imports SBSftpCommon
Imports System.Data.SqlClient
'Imports System.Text.RegularExpressions

Module Main

  Sub Main()
    Dim cn As SqlConnection = New SqlConnection("Data Source=VWHEMDMPR11\PR839;Initial Catalog=GIC;uid=remote;pwd=remote;")
    Dim cmd As SqlCommand = New SqlCommand("select max(dt) as max_dt from GIC.dbo.Desjardins_Log", cn)
    cn.Open()
    Dim rd As SqlDataReader = cmd.ExecuteReader()
    Try
      If rd.Read Then
        If rd(0).ToString().Length > 0 _
          AndAlso Date.Parse(rd(0).ToString()) > Today() Then Return
      End If
    Finally
      cmd.Dispose()
      cn.Close()
      cn.Dispose()
    End Try


    'DesjardinsRead.readFromExcel("CPG_PCB_2014-12-09.xls", "D:\Temp\")

    Desjardins()
  End Sub

  Sub Desjardins()
    'Dim ex As Exception
    'Dim exMessage As String
    'Dim exSource As String = ""
    Dim mailMessage = ""
    Dim fileName As String = ""
    Const localPath As String = "\\ngco.com\dfsroot01\DFSPCB\DFSPCB05\Decision Support\Data Group\Data\GIC\Desjardins\FromDesjardins\"

    Dim sftp As Sftp = New Sftp()
    sftp.remotePath = "/"
    sftp.localPath = localPath
    sftp.username = "P_Desjardins"
    sftp.password = "TffNC2LG"
    sftp.address = "lclsftp1.ngco.com"

    Dim AListing As ArrayList = Nothing
    'Try
    AListing = sftp.List()
    'Catch e As Exception
    'End Try
    If sftp.exMessage.Length > 0 Then
      mailMessage += ("SFtp Error" & "<br/>" & sftp.exMessage)
      Email.SendEmail("Desjardins GIC Error", mailMessage)
    End If

    If Not AListing Is Nothing AndAlso AListing.Count > 0 Then
      For Each obj As Object In AListing
        Try
          fileName = CType(obj, TElSftpFileInfo).Name
          sftp.Download(fileName)
          DesjardinsRead.readFromExcel(fileName, sftp.localPath)

          DesjardinsRead.sftpToCannex(DesjardinsRead.outputFile)

          Email.SendEmail("Desjardins GIC", _
                "File """ & fileName & """ is converted successfully." & _
                "<br/>Input File: " & sftp.localPath & fileName & _
                "<br/>Output File: " & DesjardinsRead.outputPath & DesjardinsRead.outputFile, _
                ccEmail:="PCFDataGovernance@pcbank.ca")

          Dim cn As SqlConnection = New SqlConnection("Data Source=VWHEMDMPR11\PR839;Initial Catalog=GIC;uid=remote;pwd=remote;")
          Dim cmd As SqlCommand = New SqlCommand("Insert dbo.Desjardins_Log select getdate()", cn)
          cn.Open()
          cmd.ExecuteNonQuery()
          cmd.Dispose()
          cn.Close()
          cn.Dispose()

        Catch e As Exception
          Dim inputFile As String = localPath + fileName
          If e.Source <> "SFtp" Then
            IO.File.Delete(localPath + "Invalid\" + fileName)
            IO.File.Move(localPath + fileName, localPath + "Invalid\" + fileName)
            inputFile = localPath + "Invalid\" + fileName
          End If

          mailMessage = "File """ & fileName & """ conversion failed."
          mailMessage += ("<br/>Input File: " & inputFile)
          mailMessage += ("<br/>" & e.Source & " Error" & "<br/>" & e.Message)
          If e.Source <> "SFtp" And e.Source <> "Import" And e.Source <> "Export" _
             And e.Source <> "Deposit" And e.Source <> "" Then
            mailMessage = "Unhandled Exception<br/><br/>" & mailMessage
          End If
          Email.SendEmail("Desjardins GIC Error", mailMessage)
        End Try
      Next
    Else
      If Not AListing Is Nothing And sftp.exMessage.Length = 0 Then
        Email.SendEmail("Desjardins GIC", "No Blotter from Desjardins.")
      End If
    End If

  End Sub
End Module
